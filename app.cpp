#include "app.hpp"
#include "defs.hpp"
#include<iostream>
#include<cstdlib>

#include "0IMP_test_game.hpp"

namespace ossbhf{

    App::App():
        win(DEFAULT_VIDEO_MODE,WINDOW_TITLE)
    {
        static bool constructed(false);
        if(constructed){
            App::panic("App()");
        }
        constructed = true;

        win.setFramerateLimit(FPS_LIMIT);
        win.setKeyRepeatEnabled(false);
        //TODO app initialization here
    }

    App &App::instance(){
        static App instance;
        return instance;
    }

    void App::panic(const char *msg){
        std::cerr << "ERROR: " << msg << std::endl;
        std::exit(EXIT_FAILURE);
    }

    void App::run(){
/*
 *        sf::Sprite sp(txm.tex[0]);
 *        sp.setOrigin(sf::Vector2f(txm.tex[0].getSize()) / 2.0f);
 *        sp.setPosition(100.0f,100.0f);
 *        sp.setRotation(90);
 *
 *        while(win.isOpen()){
 *            sf::Event ev;
 *            while(win.pollEvent(ev)){
 *                if(sf::Event::Closed == ev.type)
 *                    win.close();
 *                if(sf::Event::KeyPressed == ev.type &&
 *                        sf::Keyboard::Escape == ev.key.code)
 *                    win.close();
 *            }
 *
 *            win.clear(sf::Color::White);
 *            win.draw(sp);
 *            win.display();
 *        }
 */
        //TODO:app logic here
        TESTGAME::TestGameManager gm;
        gm.appRun();
    }

}


#ifndef __HGURAD_DDBULLET_OSSBHF_
#define __HGURAD_DDBULLET_OSSBHF_

#include "baseBullet.hpp"
#include <json/json.h>

namespace ossbhf{

    class DDBullet:public Bullet{
        public:
            DDBullet(class Level *lvl,
                    const Json::Value &v,
                    const sf::Vector2f &_initialPos
                    );
    };

}

#endif /* __HGURAD_DDBULLET_OSSBHF_ */

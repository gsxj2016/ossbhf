#include "baseEffect.hpp"
#include <iostream>

namespace ossbhf{

    //class Effect
    bool Effect::isDone() const{ return false; }

    //class SpriteEffect
    SpriteEffect::SpriteEffect(const sf::Texture &tex):
        spr(tex)
    {
        spr.setOrigin(sf::Vector2f(tex.getSize()) / 2.0f);
    }

    void SpriteEffect::update(sf::Time) { }

    void SpriteEffect::render(sf::RenderTarget &rt,const sf::RenderStates &rs) const{
        rt.draw(spr,rs);
    }

}

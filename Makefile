CXXFLAGS := \
    -std=c++14           \
    -O2                  \
    -ffast-math          \
    -Wall                \
    -Wnon-virtual-dtor   \
    -Woverloaded-virtual \
    -Wconversion         \
    -Werror=vla          \
    -Werror=uninitialized

LIBS := -lsfml-system -lsfml-window -lsfml-graphics -ljsoncpp
EXEC := ossbhdemo
DEPS := Makefile.depends

OBJS := \
    main.o            \
    textureManager.o  \
    defs.o            \
    objs.o            \
    entity.o          \
    basePlayer.o      \
    baseEnemy.o       \
    baseBullet.o      \
    baseCollectable.o \
    baseEffect.o      \
    playerMisc.o      \
    levelDirector.o   \
    gui.o             \
    DDEnemy.o         \
    DDBullet.o        \
    DDPlayer.o        \
    DDLevel.o         \
    0IMP_test_game.o  \
    app.o


all:$(EXEC)

clean:
	$(RM) -f -v $(EXEC) *.o

rm:
	$(RM) -f -v $(EXEC) *.o $(DEPS)

include $(DEPS)

$(EXEC):$(DEPS) $(OBJS)
	$(CXX) $(CXXFLAGS) -o $(EXEC) $(OBJS) $(LIBS)

%.o:%.cpp
	$(CXX) $(CXXFLAGS) -c $<

$(DEPS):$(wildcard *.cpp)
	$(CXX) $(CXXFLAGS) -MM *.cpp >$(DEPS)

.PHONY:all clean rm

#include "DDPlayer.hpp"
#include "DDBullet.hpp"
#include "levelDirector.hpp"
#include "app.hpp"
#include <memory>
#include <sstream>

namespace ossbhf{

    DDPlayer::DDPlayer(Level *lvl,const Json::Value &v):
        Player(lvl,App::instance().txm.t[v["img"].asString()],
                v["hit"   ].asFloat(),
                v["graze" ].asFloat(),
                v["dspeed"].asFloat(),
                v["speed" ].asFloat(),
                v["fspeed"].asFloat(),
                v["hp"  ].asInt(),
                v["life"].asInt(),
                sf::seconds(v["tshoot" ].asFloat()),
                sf::seconds(v["fade"   ].asFloat()),
                sf::seconds(v["protect"].asFloat())
              ),
        bulletSpecs(v["bullets"])
    { }

    void DDPlayer::shootBullet(){
        sf::Vector2f pos = spr.getPosition();
        for(auto &v : bulletSpecs){
            currentLevel->playerShootBullet(
                    std::make_unique<DDBullet>(currentLevel,v,pos)
                    );
        }
    }

    void DDPlayer::drawHUD(){
        std::ostringstream ss;
        ss << "Extra life: " << this->extraLife;
        playerHud.text(sf::Vector2f(512.f,0.f),false,ss.str(),14);
    }

}

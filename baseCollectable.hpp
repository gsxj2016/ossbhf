#ifndef __HGUARD_BASECOLLECTABLE_OSSBHF_
#define __HGUARD_BASECOLLECTABLE_OSSBHF_

#include "entity.hpp"

namespace ossbhf{

    class Collectable:public AutoDestructEntity{
        protected:
            float collectRange;
            sf::Vector2f velocity;
            virtual void collected(class Player &pl) =0;

        public:
            virtual void collectThis(class Player &pl);
            virtual void update(sf::Time et) override;
    };

}

#endif /* __HGUARD_BASECOLLECTABLE_OSSBHF_ */

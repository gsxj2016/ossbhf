#ifndef __HGUARD_OSSBHF_DDLEVEL_
#define __HGUARD_OSSBHF_DDLEVEL_

#include "levelDirector.hpp"
#include <json/json.h>
#include <queue>

namespace ossbhf{

    class DDLevel:public Level{
        private:
            std::queue<Json::Value> waves;

            virtual void deployEnemy() override;

        public:
            DDLevel(const Json::Value &v);
    };

}

#endif /* __HGUARD_OSSBHF_DDLEVEL_ */

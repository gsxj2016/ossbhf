#ifndef __HGUARD_TEXTURE_MANAGER_OSSBHF_
#define __HGUARD_TEXTURE_MANAGER_OSSBHF_

#include "sfinc.hpp"
#include "objs.hpp"
#include <unordered_map>
#include <string>

//TODO: set max texture number here!
#define MAX_TEXTURES 64

namespace ossbhf{
    class TextureManager final:public BaseNoCopy{
        public:
            std::unordered_map<std::string,sf::Texture> t;
            TextureManager();
    };
}

#endif /* __HGUARD_TEXTURE_MANAGER_OSSBHF_ */


namespace ossbhf{

    constexpr const float MINIMAL_PROTECTION = 0.05f;

    constexpr const float DEFAULT_HIT_RADIUS = 3.0f;

    constexpr const int MINIMAL_HP_DMG = 1;
    constexpr const int DEFAULT_COLLISON_DMG = 1;

}

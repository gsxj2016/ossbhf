#include "DDEnemy.hpp"
#include "DDBullet.hpp"
#include "DDUtils.hpp"
#include "DDDefaults.hpp"
#include "levelDirector.hpp"
#include "defs.hpp"
#include "app.hpp"
#include <cfloat>
#include <memory>

using std::make_unique;
namespace ossbhf{

    DDMovePhase::DDMovePhase(const Json::Value &val) {
        if(val.isNumeric()){
            isDelay = true;
            c = val.asFloat();
        } else {
            isDelay = false;
            v = getVec(val);
            c = val[2].asFloat();
        }
    }

    DDEnemy::DDEnemy(Level *lvl,const Json::Value &v):
        Enemy(lvl,App::instance().txm.t[v["img"].asString()],
                v.get("hp",     MINIMAL_HP_DMG).asInt(),
                v.get("cdamage",DEFAULT_COLLISON_DMG).asInt(),
                v.get("cself",  DEFAULT_COLLISON_DMG).asInt(),
                -1.0f,  //hitRadius, init later
                sf::seconds(v.get("protect",MINIMAL_PROTECTION).asFloat()),
                sf::Vector2f()),
        needRetarget(true),
        moveDelay(sf::Time::Zero),
        phaseDelay(sf::Time::Zero)
    {
        hitRadius = float(spr.getTexture()->getSize().x) / 2.0f;

        const Json::Value &vpath = v["path"];
        const Json::Value &vphase = v["attack"];

        for(auto &phase : vphase)
            phases.push_back(phase);
        nextPhase = phases.begin();

        for(auto &pathItem : vpath)
            path.emplace(pathItem);

        //first point -> initial position
        if(path.empty() || path.front().isDelay){
            dead = 1; 
        }else{
            spr.setPosition(path.front().v);
        }
    }

    void DDEnemy::normalMove(sf::Time et) {
        sf::Vector2f shift = velocity * et.asSeconds();
        spr.move(shift);

        if(!needRetarget && !path.empty()){
            const DDMovePhase &mp = path.front();
            if(mp.isDelay){
                moveDelay -= et;
                if(moveDelay <= sf::Time::Zero) needRetarget = true;
            }else{
                sf::Vector2f pos = spr.getPosition();
                if(__squaredLength(pos+shift-mp.v) > __squaredLength(pos-mp.v))
                    needRetarget = true;
            }
        }

        if(needRetarget){
            if(!path.empty())path.pop();
            needRetarget = false;
            if(!path.empty()){
                const DDMovePhase &mp = path.front();
                if(mp.isDelay){
                    velocity = sf::Vector2f();
                    moveDelay = sf::seconds(mp.c);
                }else{
                    velocity = mp.v - spr.getPosition();
                    if(__squaredLength(velocity) > FLT_EPSILON){
                        velocity /= len(velocity);
                        velocity *= mp.c;
                    }
                }
            }
        }
    }

    void DDEnemy::enemyAction(sf::Time et) {
        if(phases.empty())return;
        if(phaseDelay > sf::Time::Zero){
            phaseDelay -= et;
            return;
        }

        const DDAttackPhase &np = *nextPhase;
        const DDAttackPhase &bs = np["bullets"];
        phaseDelay = sf::seconds(np["delay"].asFloat());
        for(auto &bullet : bs){
            currentLevel->enemyShootBullet(make_unique<DDBullet>(
                        currentLevel,
                        bullet,
                        spr.getPosition()
                        ));
        }

        ++nextPhase;
        if(nextPhase == phases.end())
            nextPhase = phases.begin();
    }

}

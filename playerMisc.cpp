#include "playerMisc.hpp"
#include "app.hpp"
#include <algorithm>
#include <iostream>

namespace ossbhf{

    //class VisualHitPt
    VisualHitPt::VisualHitPt(float r){
        pt.setFillColor(sf::Color(255,64,64));
        pt.setRadius(r);
        pt.setOrigin(r,r);
    }

    void VisualHitPt::render(sf::RenderTarget &rt,const sf::Transform &tr)const{
        rt.draw(pt,tr);
    }

    //class FocusEffect
    FocusEffect::FocusEffect():
        SpriteEffect(App::instance().txm.t["effect_focus"])
    { }
    
    void FocusEffect::update(sf::Time et){
        spr.rotate(ROTATE_SPEED * et.asSeconds());
    }

    //class InvulnerableEffect
    InvulnerableEffect::InvulnerableEffect():
        SpriteEffect(App::instance().txm.t["effect_invul"])
    { }

}

#ifndef __HGUARD_BASEPLAYER_OSSBHF_
#define __HGUARD_BASEPLAYER_OSSBHF_

#include "entity.hpp"
#include "playerMisc.hpp"
#include "gui.hpp"

namespace ossbhf{

    class Player:public Entity{
        public:
            enum State{
                state_deploy,
                state_normal,
                state_fading,
                state_dead
            };

        protected:
            GUI playerHud;
            float hitRadius;
            float grazeRadius;
            float deploySpeed;
            float normalSpeed;
            float focusSpeed;
            int initialHp;
            int extraLife;
            sf::Time shootInterval;
            sf::Time defaultFadingTime;
            sf::Time protectionTime;

            int hp;
            int pendingMiss;
            bool skillInhibitShoot;
            bool focus;
            bool shooting;
            sf::Time invulnerable;
            sf::Time fadingTime;
            sf::Time shootWait;
            sf::Vector2i moveState;
            State state;
            VisualHitPt hitPt;
            FocusEffect focusEffect;
            InvulnerableEffect invulnerableEffect;

            //change State
            void lost();
            virtual void beforeMiss(int atk);
            virtual void actualMiss();
            virtual void deploy();
            //updates
            virtual void deploying(sf::Time et);
            virtual void fading(sf::Time et);
            virtual void normalRunning(sf::Time et);
            //internals
            virtual void doMove(sf::Time et);
            virtual void doShoot(sf::Time et);
            virtual void doInvCountdown(sf::Time et);
            virtual void doFadeCountdown(sf::Time et);
            virtual void checkPlayerControl();
            virtual void doPreRenderChange(sf::Time et);
            virtual void skillUsage();
            virtual void shootBullet() =0;

            Player(class Level *lvl,const sf::Texture &tex,
                    float _hitrad, float _grazerad,
                    float _deployspd, float _normalspd, float _focusspd,
                    int _hp, int _life,
                    sf::Time _shootiv,
                    sf::Time _fade,
                    sf::Time _protect
                  );

        public:
            virtual void damage(int atk);
            virtual void enchantInvulnerability(sf::Time ivt);
            bool hitTest(const sf::Vector2f &pos,float range) const;
            bool grazeTest(const sf::Vector2f &pos,float range) const;
            bool isNormal() const;
            virtual void update(sf::Time et) override;
            virtual void render(sf::RenderTarget &rt) const override;
            virtual void drawHUD() =0;
    };

}

#endif /* __HGUARD_BASEPLAYER_OSSBHF_ */

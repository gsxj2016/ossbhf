#ifndef __HGUARD_GUI_OSSBHF_
#define __HGUARD_GUI_OSSBHF_

#include "objs.hpp"
#include "sfinc.hpp"
#include "textureManager.hpp"

namespace ossbhf{

    class GUIStatic final : public BaseNoCopy{
        public:
            sf::Font font;

            GUIStatic();
    };

    class GUI : public Object {
        private:
            sf::RenderWindow &window;
            TextureManager &textureManager;

        public:
            static const GUIStatic res;

            GUI();
            void text(const sf::Vector2f &pos,bool center,const sf::String &str,unsigned size);
            void image(const sf::Vector2f &pos,bool center,const sf::String &id);
    };

}

#endif /* __HGUARD_GUI_OSSBHF_ */

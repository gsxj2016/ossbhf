#ifndef __HGUARD_DEFS_OSSBHF_
#define __HGUARD_DEFS_OSSBHF_

#include "sfinc.hpp"
#include<cmath>

#define WINDOW_TITLE "OSSBHF Demo"
#define FONT_FILE "res/fonts/DroidSansFallback.ttf"

namespace ossbhf{

    //video settings
    constexpr const int FPS_LIMIT = 64;
    constexpr const int WINDOW_W = 640;
    constexpr const int WINDOW_H = 480;
    extern const sf::VideoMode DEFAULT_VIDEO_MODE;

    //key configuration
    constexpr const auto KEY_MOVE_LEFT  = sf::Keyboard::Left;
    constexpr const auto KEY_MOVE_RIGHT = sf::Keyboard::Right;
    constexpr const auto KEY_MOVE_UP    = sf::Keyboard::Up;
    constexpr const auto KEY_MOVE_DOWN  = sf::Keyboard::Down;
    constexpr const auto KEY_SHOOT      = sf::Keyboard::Z;
    constexpr const auto KEY_FOCUS      = sf::Keyboard::LShift;

    //stage size
    constexpr const int STAGE_W = 384;
    constexpr const int STAGE_H = 480;
    constexpr const float STAGE_VP_W = 384.f/640.f;
    constexpr const float STAGE_VP_H = 1.f;
    constexpr const float STAGE_VP_X = 128.f/640.f;
    constexpr const float STAGE_VP_Y = 0.f;

    extern const sf::FloatRect STAGE_RECT;
    extern const sf::FloatRect VIEWPORT_STAGE_RECT;
    extern const sf::View STAGE_VIEW;

    //In-game info position
    constexpr const int INGAME_INFO_BOX_X = 400;
    constexpr const int INGAME_INFO_BOX_Y = 16;
    constexpr const int INGAME_INFO_BOX_W = 224;
    constexpr const int INGAME_INFO_BOX_H = 448;
    constexpr const int INGAME_INFO_X = 416;
    constexpr const int INGAME_INFO_Y = 32;
    extern const sf::FloatRect INGAME_INFO_BOX;

    //deploy position
    constexpr const float PRE_DEP_X  = STAGE_W/2.0f;
    constexpr const float PRE_DEP_Y  = STAGE_H+32.0f;
    constexpr const float POST_DEP_X = PRE_DEP_X;
    constexpr const float POST_DEP_Y = STAGE_H-32.0f;
    extern const sf::Vector2f PRE_DEP;
    extern const sf::Vector2f POST_DEP;
    extern const sf::Vector2f DEP_DIR_ID;

    //math
    constexpr const float PI_FLOAT = float(M_PI);
    constexpr const float PI_OVER_2 = PI_FLOAT * 0.5f;
    constexpr const float TO_RAD = 180.0f / PI_FLOAT;

    //vector utility functions
    inline static float __squaredLength(const sf::Vector2f &a){
        return a.x*a.x + a.y*a.y;
    }
    inline static float len(const sf::Vector2f &a){
        return std::sqrt(__squaredLength(a));
    }

}

#endif /* __HGUARD_DEFS_OSSBHF_ */

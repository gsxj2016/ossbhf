#include "levelDirector.hpp"
#include "baseEnemy.hpp"
#include "basePlayer.hpp"
#include "baseBullet.hpp"
#include "baseCollectable.hpp"
#include "app.hpp"
#include "DDEnemy.hpp"
#include "DDPlayer.hpp"
#include "DDLevel.hpp"
#include <json/json.h>
#include <memory>
#include <iostream>
#include <fstream>
#include "0IMP_test_game.hpp"
using std::make_unique;

namespace ossbhf{

    namespace TESTGAME{

        inline static sf::Texture &getTexture(const char *name){
            return App::instance().txm.t[name];
        }

        void TestGameManager::appRun(){
            Json::Value playerData,levelData;
            {
                std::ifstream file("res/test.json");
                if(file.fail()) App::panic("failed read test.json");
                Json::Reader rd;
                Json::Value v;
                if(!rd.parse(file,v,false)) App::panic("failed parse");
                playerData = v["player"];
                levelData  = v["level"];
            }

            DDLevel tl(levelData);
            tl.playerEnterLevel(make_unique<DDPlayer>(&tl,playerData));

            sf::Clock clock;
            sf::Time fpstime;
            int fpscount = 0;
            while(tl.getLevelState() == Level::State::lstate_running){
                sf::Time elapsed = clock.restart();
                tl.update(elapsed);
                //counting fps
                ++fpscount;
                fpstime += elapsed;
                if(fpstime.asSeconds() > 10.0f){
                    std::cerr
                        << "FPS: "
                        << float(fpscount)/fpstime.asSeconds()
                        << std::endl;
                    fpstime = sf::Time::Zero;
                    fpscount = 0;
                }
                //exit
                if(sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Q)){
                    App::panic("EXIT\n");
                }
            }
        }

    }

}

#include "baseEnemy.hpp"
#include "basePlayer.hpp"
#include "defs.hpp"
#include "levelDirector.hpp"

namespace ossbhf{

    Enemy::Enemy(Level *lvl,sf::Texture &tex,
            int _hp,
            int _colDmg,
            int _colSelfDmg,
            float _hitRadius,
            sf::Time _protectionTime,
            const sf::Vector2f &_initialVelocity):
        AutoDestructEntity(lvl,tex,false),
        hp(_hp),
        collisionDamage(_colDmg),
        collisionSelfDamage(_colSelfDmg),
        hitRadius(_hitRadius),
        protectionTime(_protectionTime),
        velocity(_initialVelocity),

        aliveTime(sf::Time::Zero),invulnerable(sf::Time::Zero)
    { }

    void Enemy::killed(){
        dead = 1;
    }

    void Enemy::normalMove(sf::Time et){
        spr.move(et.asSeconds() * velocity);
    }

    void Enemy::update(sf::Time et){
        if(dead) return;

        //check invul time
        if(invulnerable > sf::Time::Zero){
            invulnerable -= et;
            if(invulnerable <= sf::Time::Zero)
                invulnerable = sf::Time::Zero;
        }

        //move && shoot
        normalMove(et);
        enemyAction(et);

        //cleanup
        destructLeftStageEntity();
    }

    void Enemy::collide(Player &pl){
        if(dead) return;
        if(!pl.isNormal()) return;

        if( pl.hitTest(spr.getPosition(),hitRadius) ){
            pl.damage(collisionDamage);
            damage(collisionSelfDamage);
        }
    }

    void Enemy::damage(int atk){
        if(dead) return;
        if(!atk) return;
        if(invulnerable > sf::Time::Zero) return;

        hp -= atk;
        if(hp <= 0){
            killed();
        }else{
            invulnerable = protectionTime;
        }
    }

    bool Enemy::hitTest(const sf::Vector2f &pos,float range) const{
        return (!dead)
            && len(pos-spr.getPosition()) < range + hitRadius;
    }

}

#ifndef __HGUARD_BASEEFFECT_OSSBHF_
#define __HGUARD_BASEEFFECT_OSSBHF_

#include "objs.hpp"
#include "sfinc.hpp"
namespace ossbhf{

    class Effect:public Object{
        public:
            virtual bool isDone() const;
            virtual void update(sf::Time et) =0;
            virtual void render(
                    sf::RenderTarget &rt,
                    const sf::RenderStates &rs = sf::RenderStates()
                    ) const =0;
    };

    class SpriteEffect:public Effect{
        protected:
            sf::Sprite spr;

        public:
            virtual void update(sf::Time et) override;
            virtual void render(
                    sf::RenderTarget &rt,
                    const sf::RenderStates &rs = sf::RenderStates()
                    ) const override;

            SpriteEffect(const sf::Texture &tex);
    };

}

#endif /* __HGUARD_BASEEFFECT_OSSBHF_ */

#ifndef __HGUARD_BASEENEMY_OSSBHF_
#define __HGUARD_BASEENEMY_OSSBHF_

#include "entity.hpp"

namespace ossbhf{

    class Enemy:public AutoDestructEntity{
        protected:
            int hp;
            int collisionDamage;
            int collisionSelfDamage;
            float hitRadius;
            sf::Time protectionTime;
            sf::Vector2f velocity;

            sf::Time aliveTime;
            sf::Time invulnerable;

            virtual void killed();
            virtual void normalMove(sf::Time et);
            virtual void enemyAction(sf::Time et) =0;

            Enemy(class Level *lvl,sf::Texture &tex,
                    int _hp,
                    int _colDmg,
                    int _colSelfDmg,
                    float _hitRadius,
                    sf::Time _protectionTime,
                    const sf::Vector2f &_initialVelocity);

        public:
            virtual void damage(int atk);
            virtual void collide(class Player &pl);
            bool hitTest(const sf::Vector2f &pos,float range) const;
            virtual void update(sf::Time et) override;
    };
}

#endif /* __HGUARD_BASEENEMY_OSSBHF_ */

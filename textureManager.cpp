#include "textureManager.hpp"
#include "app.hpp"
#include <json/json.h>
#include <fstream>

#define ASSETSFILE "res/assets.json"

namespace ossbhf{

    TextureManager::TextureManager(){
        std::ifstream file(ASSETSFILE);
        if(file.fail()){
            App::panic("failed open " ASSETSFILE);
        }

        Json::Reader rd;
        Json::Value root;

        if(rd.parse(file,root,false)){
            for(Json::ValueIterator it=root.begin();it!=root.end();++it){
                if(!t[it.name()].loadFromFile(it->asCString())){
                    App::panic("failed loading texture");
                }
            }
        }else{
            App::panic("failed parsing " ASSETSFILE);
        }

    }

}

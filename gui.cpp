#include "gui.hpp"
#include "defs.hpp"
#include "app.hpp"

namespace ossbhf{

    //class GUIStatic
    GUIStatic::GUIStatic(){
        if(!font.loadFromFile(FONT_FILE)){
            App::panic("Failed loading font.");
        }
    }

    //class GUI
    const GUIStatic GUI::res;

    GUI::GUI():
        window         (App::instance().win),
        textureManager (App::instance().txm)
    { }

    void GUI::text(const sf::Vector2f &pos,bool center,const sf::String &str,unsigned size){
        sf::Text text(str,res.font,size);
        sf::FloatRect box(text.getLocalBounds());
        if(center){
            text.setOrigin(box.width*0.5f,box.height*0.5f);
        }
        text.setPosition(pos);
        text.setColor(sf::Color::Black);
        window.draw(text);
    }

    void GUI::image(const sf::Vector2f &pos,bool center,const sf::String &id){
        const sf::Texture &img = textureManager.t[id];
        sf::Sprite sprite(img);
        if(center){
            sprite.setOrigin(sf::Vector2f(img.getSize())*0.5f);
        }
        sprite.setPosition(pos);
        window.draw(sprite);
    }

}

#include "entity.hpp"
#include "defs.hpp"
#include "levelDirector.hpp"

namespace ossbhf{

    //Entity base class
    bool Entity::isDead() const { return dead; }
    bool Entity::isAlive() const { return !dead; }

    Entity::Entity(Level *lvl,const sf::Texture &tex):
        currentLevel(lvl),
        spr(tex),
        dead(0)
    {
        spr.setOrigin(sf::Vector2f(tex.getSize()) / 2.0f);
    }

    void Entity::render(sf::RenderTarget &target) const {
        if(!dead){
            target.draw(spr);
        }
    }

    //AutoDestructEntity base class
    AutoDestructEntity::AutoDestructEntity(Level *lvl,const sf::Texture &tex,
            bool initialDeployed):
        Entity(lvl,tex),
        deployed(initialDeployed)
    {}

    void AutoDestructEntity::destructLeftStageEntity(){
        if( STAGE_RECT.intersects(spr.getGlobalBounds()) ){
            if(!deployed)
                deployed = 1;
        } else {
            if(deployed)
                dead = 1;
        }
    }

}

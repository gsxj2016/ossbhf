#ifndef __HGUARD_ENTITY_OSSBHF_
#define __HGUARD_ENTITY_OSSBHF_

#include "sfinc.hpp"
#include "objs.hpp"

namespace ossbhf{

    class Entity:public Object{
        protected:
            class Level *currentLevel;
            sf::Sprite spr;
            bool dead;

            Entity(class Level *lvl,const sf::Texture &tex);

        public:
            bool isDead() const;
            bool isAlive() const;
            virtual void render(sf::RenderTarget &target) const;
            virtual void update(sf::Time et) =0;
    };

    class AutoDestructEntity:public Entity{
        protected:
            virtual void destructLeftStageEntity();
            AutoDestructEntity(class Level *lvl,const sf::Texture &tex,bool initialDeployed);

        private:
            bool deployed;
    };

}

#endif /* __HGUARD_ENTITY_OSSBHF_ */

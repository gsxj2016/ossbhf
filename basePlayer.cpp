#include "basePlayer.hpp"
#include "defs.hpp"
#include "app.hpp"

namespace ossbhf{

    Player::Player(Level *lvl,const sf::Texture &tex,
            float _hitrad, float _grazerad,
            float _deployspd, float _normalspd, float _focusspd,
            int _hp, int _life,
            sf::Time _shootiv,
            sf::Time _fade,
            sf::Time _protect
            ):
        Entity(lvl,tex),
        hitRadius   (_hitrad),
        grazeRadius (_grazerad),
        deploySpeed (_deployspd),
        normalSpeed (_normalspd),
        focusSpeed  (_focusspd),
        initialHp   (_hp),
        extraLife   (_life),
        shootInterval    (_shootiv),
        defaultFadingTime(_fade),
        protectionTime   (_protect),
        hp(0),pendingMiss(0),skillInhibitShoot(0),focus(0),shooting(0),
        state(state_dead),
        hitPt(hitRadius)
    {
        deploy();
    }

    void Player::lost(){
        dead  = 1;
        state = state_dead;
    }

    void Player::deploy(){
        shootWait = invulnerable = fadingTime = sf::Time::Zero;
        hp = initialHp;
        spr.setPosition(PRE_DEP);
        state = state_deploy;
    }

    void Player::beforeMiss(int atk){
        pendingMiss = atk;
        state = state_fading;
        fadingTime = defaultFadingTime;
    }

    void Player::actualMiss(){
        hp -= pendingMiss;
        state = state_normal;
        if(hp <= 0){
            if(extraLife){
                --extraLife;
                deploy();
            }else{
                lost();
            }
        }
    }

    void Player::deploying(sf::Time et){
        float maxDist = et.asSeconds() * deploySpeed;
        if(len(POST_DEP-spr.getPosition()) <= maxDist){
            spr.setPosition(POST_DEP);
            state = state_normal;
            invulnerable = protectionTime;
        }else{
            spr.move(DEP_DIR_ID * maxDist);
        }
    }

    void Player::fading(sf::Time et){
        skillUsage();
        checkPlayerControl();
        doMove(et);
        doFadeCountdown(et);
    }

    void Player::normalRunning(sf::Time et){
        skillUsage();
        checkPlayerControl();
        doMove(et);
        doShoot(et);
        doInvCountdown(et);
    }

    void Player::doFadeCountdown(sf::Time et){
        fadingTime -= et;
        if(fadingTime <= sf::Time::Zero){
            fadingTime = sf::Time::Zero;
            actualMiss();
        }
    }

    void Player::doInvCountdown(sf::Time et){
        if(invulnerable > sf::Time::Zero){
            invulnerable -= et;
            if(invulnerable <= sf::Time::Zero){
                invulnerable = sf::Time::Zero;
            }
        }
    }

    void Player::doMove(sf::Time et){
        if(sf::Vector2i(0,0) != moveState){
            sf::Vector2f shift(moveState);
            shift *= (focus ? focusSpeed : normalSpeed) / len(shift);
            shift *= et.asSeconds();

            sf::Vector2f target(spr.getPosition() + shift);
            if(target.x < 0.0f) target.x = 0.0f;
            if(target.y < 0.0f) target.y = 0.0f;
            if(target.x > STAGE_W) target.x = STAGE_W;
            if(target.y > STAGE_H) target.y = STAGE_H;

            spr.setPosition(target);
        }
    }

    void Player::doShoot(sf::Time et){
        if(shootWait > sf::Time::Zero){
            shootWait -= et;
        }
        if(shooting && !skillInhibitShoot){
            if(shootWait <= sf::Time::Zero){
                shootBullet();
                shootWait += shootInterval;
            }
        }
    }

    void Player::checkPlayerControl(){
        //move
        moveState.x = moveState.y = 0;
        if(sf::Keyboard::isKeyPressed(KEY_MOVE_LEFT )) moveState.x -= 1;
        if(sf::Keyboard::isKeyPressed(KEY_MOVE_RIGHT)) moveState.x += 1;
        if(sf::Keyboard::isKeyPressed(KEY_MOVE_UP   )) moveState.y -= 1;
        if(sf::Keyboard::isKeyPressed(KEY_MOVE_DOWN )) moveState.y += 1;
        //focus & shoot
        shooting = sf::Keyboard::isKeyPressed(KEY_SHOOT);
        focus    = sf::Keyboard::isKeyPressed(KEY_FOCUS);
    }

    void Player::doPreRenderChange(sf::Time et){
        focusEffect.update(et);
        spr.setColor(sf::Color::White);

        if(state_normal == state){
            if(invulnerable > sf::Time::Zero){
                spr.setColor(sf::Color(255,255,255,192));
            }
            if(focus){
                spr.setColor(sf::Color(255,255,255,128));
            }
        }
    }

    void Player::skillUsage(){
        skillInhibitShoot = 0;
    }

    void Player::update(sf::Time et){
        if(dead) return;
        switch(state){
            case state_deploy:
                deploying(et);
                break;
            case state_normal:
                normalRunning(et);
                break;
            case state_fading:
                fading(et);
                break;
            case state_dead:
                App::panic("BUG upd dead player");
                break;
        }
        doPreRenderChange(et);
    }

    bool Player::hitTest(const sf::Vector2f &pos,float range) const{
        return (!dead)
            && len(pos-spr.getPosition()) < range + hitRadius;
    }

    bool Player::grazeTest(const sf::Vector2f &pos,float range) const{
        return (!dead)
            && len(pos-spr.getPosition()) < range + hitRadius + grazeRadius;
    }

    bool Player::isNormal() const{
        return State::state_normal == state;
    }

    void Player::damage(int atk){
        if(dead) return;
        if(!atk) return;
        if(invulnerable > sf::Time::Zero) return;
        if(State::state_normal != state) return;

        beforeMiss(atk);
    }

    void Player::enchantInvulnerability(sf::Time ivt){
        if(dead) return;
        if(State::state_normal != state) return;

        invulnerable = ivt;
    }

    void Player::render(sf::RenderTarget &rt) const{
        sf::Transform tr; tr.translate(spr.getPosition());

        //underlay
        if(state_normal == state){
            if(focus){
                focusEffect.render(rt,tr);
            }
            if(invulnerable > sf::Time::Zero){
                invulnerableEffect.render(rt,tr);
            }
        }else if(state_fading == state){
        }

        //self
        Entity::render(rt);

        //overlay
        if(focus){
            hitPt.render(rt,tr);
        }
    }

}

#include "baseBullet.hpp"
#include "basePlayer.hpp"
#include "baseEnemy.hpp"
#include "defs.hpp"
#include <cmath>
#include <cfloat>

namespace ossbhf{

    Bullet::Bullet(Level *lvl,sf::Texture &tex,
            int _dmg,
            float _range,
            bool _persistent,
            const sf::Vector2f &_initialVelocity,
            const sf::Vector2f &_accel,
            float _asymptotic_bonus,
            float _asymptotic_decel,
            const sf::Vector2f &_initialPos
            ):
        AutoDestructEntity(lvl,tex,true),
        dmg            (_dmg),
        range          (_range),
        persistent     (_persistent),
        velocity       (_initialVelocity),
        accel          (_accel),
        asymptoticBonus(_asymptotic_bonus),
        asymptoticDecel(_asymptotic_decel)
    {
        spr.setPosition(_initialPos);
        doRotate();
    }

    void Bullet::doRotate(){
        if(__squaredLength(velocity) > FLT_EPSILON){
            spr.setRotation( std::atan2(velocity.y,velocity.x) * TO_RAD );
        }
    }

    void Bullet::update(sf::Time et){
        if(dead) return;

        //move
        spr.move( et.asSeconds() * velocity * (1.0f + asymptoticBonus) );
        velocity += et.asSeconds() * accel;
        asymptoticBonus *= std::pow(asymptoticDecel,et.asSeconds());

        //rotate
        doRotate();

        //cleanup
        destructLeftStageEntity();
    }

    void Bullet::hitEnemy(Enemy &ent){
        if(dead)return;

        if(ent.hitTest(spr.getPosition(),range)){
            ent.damage(dmg);
            if(!persistent) dead = 1;
        }
    }

    void Bullet::hitPlayer(Player &ent){
        if(dead)return;

        if(ent.hitTest(spr.getPosition(),range)){
            ent.damage(dmg);
            if(!persistent) dead = 1;
        }

        if(!dead && ent.grazeTest(spr.getPosition(),range)){
            //TODO: graze handling
        }
    }

}

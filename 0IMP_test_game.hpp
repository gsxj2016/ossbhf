#ifndef __HGUARD_TEST_GAME_0_OSSBHF
#define __HGUARD_TEST_GAME_0_OSSBHF

#include "objs.hpp"

namespace ossbhf{
    namespace TESTGAME{

        class TestGameManager final:public BaseNoCopy{
            public:
                void appRun();
        };

    }
}
#endif /* __HGUARD_TEST_GAME_0_OSSBHF */

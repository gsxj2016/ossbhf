#include "baseCollectable.hpp"
#include "defs.hpp"
#include "basePlayer.hpp"

namespace ossbhf{

    void Collectable::update(sf::Time et){
        if(dead) return;

        //move
        spr.move(et.asSeconds() * velocity);

        //clean
        destructLeftStageEntity();
    }

    void Collectable::collectThis(Player &pl){
        if(dead) return;

        if( pl.hitTest(spr.getPosition(),collectRange) ){
            collected(pl);
            dead = 1;
        }
    }

}

#ifndef __HGUARD_DDUTILS_OSSBHF_
#define __HGUARD_DDUTILS_OSSBHF_

#include "sfinc.hpp"
#include <json/json.h>
namespace ossbhf{

    inline static sf::Vector2f getVec(const Json::Value &v){
        if(v.isNull()) return sf::Vector2f();
        return sf::Vector2f(v[0].asFloat(),v[1].asFloat());
    }

}

#endif /* __HGUARD_DDUTILS_OSSBHF_ */

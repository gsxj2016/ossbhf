#ifndef __HGURAD_DDPLAYER_OSSBHF_
#define __HGURAD_DDPLAYER_OSSBHF_

#include "basePlayer.hpp"
#include <json/json.h>
namespace ossbhf{

    class DDPlayer:public Player{
        public:
            DDPlayer(class Level *lvl,const Json::Value &v);

        private:
            Json::Value bulletSpecs;

            virtual void shootBullet() override;
            virtual void drawHUD() override;
    };

}

#endif /* __HGURAD_DDPLAYER_OSSBHF_ */

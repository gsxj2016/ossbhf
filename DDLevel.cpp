#include "DDLevel.hpp"
#include "DDEnemy.hpp"
#include "baseCollectable.hpp"
#include "baseBullet.hpp"
#include "basePlayer.hpp"
#include <memory>

namespace ossbhf{

    DDLevel::DDLevel(const Json::Value &v){
        const Json::Value &theWaves = v["wave"];
        for(auto &i : theWaves){
            waves.push(i);
        }
    }

    void DDLevel::deployEnemy(){
        //TODO:dialog,etc...
        if(waves.empty()){
            //TODO: level cleared
            levelEnd = true;
        }else{
            const Json::Value &currentWave = waves.front();
            allowNextWave = currentWave["next"].asBool();
            nextWaveDelay = sf::seconds(currentWave["delay"].asFloat());

            const Json::Value &waveEnemies = currentWave["enemy"];
            for(auto &i : waveEnemies){
                enemies.push_back(std::make_unique<DDEnemy>(this,i));
            }

            waves.pop();
        }
    }

}

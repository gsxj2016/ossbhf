#!/bin/bash

rm -vf *.png
for i in *.svg
do
    BASENAME=$(basename --suffix=.svg ${i})
    convert -background none ${BASENAME}.svg ${BASENAME}.png
done

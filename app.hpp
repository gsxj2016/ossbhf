#ifndef __HGUARD_APP_OSSBHF_
#define __HGUARD_APP_OSSBHF_

#include "textureManager.hpp"
#include "objs.hpp"

namespace ossbhf{

    class App final:public BaseNoCopy{
        public:
            TextureManager txm;
            sf::RenderWindow win;

            static App &instance();
            static void panic(const char *msg);

            void run();

        private:
            App();
    };

}

#endif /* __HGUARD_APP_OSSBHF_ */

#ifndef __HGUARD_BASEBULLET_OSSBHF_
#define __HGUARD_BASEBULLET_OSSBHF_

#include "entity.hpp"

namespace ossbhf{

    class Bullet:public AutoDestructEntity{
        protected:
            int dmg;
            float range;
            bool persistent;
            sf::Vector2f velocity;
            sf::Vector2f accel;
            float asymptoticBonus;
            float asymptoticDecel;

            void doRotate();

        public:
            virtual void hitPlayer(class Player &ent);
            virtual void hitEnemy (class Enemy  &ent);
            virtual void update(sf::Time et) override;

            Bullet(class Level *lvl,sf::Texture &tex,
                    int _dmg,
                    float _range,
                    bool _persistent,
                    const sf::Vector2f &_initialVelocity,
                    const sf::Vector2f &_accel,
                    float _asymptotic_bonus,
                    float _asymptotic_decel,
                    const sf::Vector2f &_initialPos
                  );
    };

}

#endif /* __HGUARD_BASEBULLET_OSSBHF_ */

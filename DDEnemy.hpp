#ifndef __HGUARD_DDENEMY_OSSBHF_
#define __HGUARD_DDENEMY_OSSBHF_

#include "objs.hpp"
#include "baseEnemy.hpp"
#include <utility>
#include <queue>
#include <list>
#include <json/json.h>

namespace ossbhf{

    typedef Json::Value DDAttackPhase;

    class DDMovePhase final:public BaseNoCopy{
        public:
            bool isDelay;
            sf::Vector2f v;
            float c;

            explicit DDMovePhase(const Json::Value &val);
    };

    class DDEnemy:public Enemy{
        public:
            DDEnemy(class Level *lvl,const Json::Value &v);

        private:
            std::queue<DDMovePhase> path;
            std::list<DDAttackPhase> phases;
            std::list<DDAttackPhase>::iterator nextPhase;
            bool needRetarget;
            sf::Time moveDelay;
            sf::Time phaseDelay;

            virtual void normalMove(sf::Time et) override;
            virtual void enemyAction(sf::Time et) override;
    };

}

#endif /* __HGUARD_DDENEMY_OSSBHF_ */

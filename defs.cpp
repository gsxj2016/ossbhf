#include "defs.hpp"

namespace ossbhf{

    //get a sf::View from (area,viewport)
    inline static sf::View getView(const sf::FloatRect &vi,const sf::FloatRect &vp){
        sf::View v(vi);
        v.setViewport(vp);
        return v;
    }

    //video
    const sf::VideoMode DEFAULT_VIDEO_MODE{WINDOW_W,WINDOW_H,32};

    //in-game info
    const sf::FloatRect INGAME_INFO_BOX{INGAME_INFO_BOX_X,INGAME_INFO_BOX_Y,
                                        INGAME_INFO_BOX_W,INGAME_INFO_BOX_H};

    //stage
    const sf::FloatRect STAGE_RECT{0,0,STAGE_W,STAGE_H};
    const sf::FloatRect VIEWPORT_STAGE_RECT{STAGE_VP_X,STAGE_VP_Y
        ,                                   STAGE_VP_W,STAGE_VP_H};
    const sf::View STAGE_VIEW{getView(STAGE_RECT,VIEWPORT_STAGE_RECT)};

    //deploy
    const sf::Vector2f PRE_DEP {PRE_DEP_X ,PRE_DEP_Y};
    const sf::Vector2f POST_DEP{POST_DEP_X,POST_DEP_Y};
    const sf::Vector2f DEP_VECTOR{POST_DEP-PRE_DEP};
    const sf::Vector2f DEP_DIR_ID{DEP_VECTOR / len(DEP_VECTOR)};

}

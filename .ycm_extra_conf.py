import os
import ycm_core

flags = [
'-std=c++14',
'-Wall',
'-Wnon-virtual-dtor',
'-Woverloaded-virtual',
'-Wconversion',
'-Werror=uninitialized'
]

def FlagsForFile( filename ):
  return {
    'flags': flags,
    'do_cache': True
  }

#ifndef __HGUARD_PLAYERMISC_OSSBHF_
#define __HGUARD_PLAYERMISC_OSSBHF_

#include "sfinc.hpp"
#include "objs.hpp"
#include "baseEffect.hpp"

namespace ossbhf{

    class VisualHitPt final:public BaseNoCopy{
        public:
            void render(sf::RenderTarget &rt,const sf::Transform &tr)const;
            VisualHitPt(float r);

        private:
            sf::CircleShape pt;

    };

    class FocusEffect:public SpriteEffect{
        private:
            static constexpr const float ROTATE_SPEED = 45.f;

        public:
            void update(sf::Time et) override;
            FocusEffect();
    };

    class InvulnerableEffect:public SpriteEffect{
        public:
            InvulnerableEffect();
    };

}

#endif /* __HGUARD_PLAYERMISC_OSSBHF_ */

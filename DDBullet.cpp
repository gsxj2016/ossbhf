#include "DDUtils.hpp"
#include "DDBullet.hpp"
#include "DDDefaults.hpp"
#include "levelDirector.hpp"
#include "app.hpp"

namespace ossbhf{

    DDBullet::DDBullet(Level *lvl,
            const Json::Value &v,
            const sf::Vector2f &_initialPos
            ):
        Bullet(lvl,App::instance().txm.t[v["img"].asString()],
                v.get("damage",MINIMAL_HP_DMG).asInt(),
                v.get("range", DEFAULT_HIT_RADIUS).asFloat(),
                v["persist"].asBool(),
                getVec(v["v"]),
                getVec(v["a"]),
                v["asym"].asFloat(),
                v["decel"].asFloat(),
                _initialPos + getVec(v["offset"])
              )
    { }

}

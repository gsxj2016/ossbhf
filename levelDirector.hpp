#ifndef __HGUARD_LEVELDIRECTOR_OSSBHF_
#define __HGUARD_LEVELDIRECTOR_OSSBHF_

#include <list>
#include <memory>
#include "objs.hpp"
#include "sfinc.hpp"
#include "gui.hpp"

namespace ossbhf{

    class LevelStatic:public BaseNoCopy{
        public:
            sf::RectangleShape sceneBox;
            
            LevelStatic();
    };

    class Level:public Object{
        public:
            enum State{
                lstate_invalid,
                lstate_running,
                lstate_failed,
                lstate_finished
            };

        private:
            const static LevelStatic S;

        protected:
            GUI hud;
            sf::Time levelTime;
            sf::Time nextWaveDelay;
            bool allowNextWave;
            bool levelEnd;

            std::list< std::unique_ptr<class Enemy >      > enemies;
            std::list< std::unique_ptr<class Bullet>      > playerBullets;
            std::list< std::unique_ptr<class Bullet>      > enemyBullets;
            std::list< std::unique_ptr<class Collectable> > droppedItems;
            std::unique_ptr<class Player> player;

            void cleanUp();
            void updateEntities(sf::Time et);
            void handleBulletHit();
            void handleEnemyCollision();
            void handleCollectItem();
            void drawEntities();
            void drawHUD();
            void drawLevel();
            virtual void deployEnemy() =0;

            Level();

        public:
            void playerShootBullet(std::unique_ptr<class Bullet> &&pb);
            void enemyShootBullet(std::unique_ptr<class Bullet> &&pb);
            void dealGlobalDamage(int dmg);
            void playerEnterLevel(std::unique_ptr<class Player> &&_pl);
            State getLevelState();
            virtual void update(sf::Time et);
    };

}

#endif /* __HGUARD_LEVELDIRECTOR_OSSBHF_ */

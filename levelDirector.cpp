#include "levelDirector.hpp"
#include "baseBullet.hpp"
#include "basePlayer.hpp"
#include "baseEnemy.hpp"
#include "app.hpp"
#include "baseCollectable.hpp"
#include "defs.hpp"
#include "gui.hpp"
#include <list>
#include <memory>

namespace ossbhf{

    //class LevelStatic
    LevelStatic::LevelStatic():
        sceneBox(sf::Vector2f(STAGE_W,STAGE_H))
    {
        sceneBox.setFillColor(sf::Color::Transparent);
        sceneBox.setOutlineColor(sf::Color::Black);
        sceneBox.setOutlineThickness(-1.f);
    }

    //class Level
    const LevelStatic Level::S;

    Level::Level():
        levelTime    (sf::Time::Zero),
        nextWaveDelay(sf::Time::Zero),
        allowNextWave(false),
        levelEnd     (true)
    { }

    void Level::cleanUp(){
        auto isDead = [](auto &x){ return x->isDead(); };
        enemies.remove_if(isDead);
        playerBullets.remove_if(isDead);
        enemyBullets.remove_if(isDead);
    }

    void Level::updateEntities(sf::Time et){
        for(auto &x : enemyBullets)  x->update(et);
        for(auto &x : playerBullets) x->update(et);
        for(auto &x : enemies)       x->update(et);
        player->update(et);
    }

    void Level::handleBulletHit(){
        //player bullet -> enemies
        for(auto &bullet : playerBullets){
            for(auto &enemy : enemies){
                if(bullet->isDead())break;
                bullet->hitEnemy(*enemy);
            }
        }
        //enemy bullet -> player
        for(auto &bullet : enemyBullets){
            bullet->hitPlayer(*player);
        }
    }

    void Level::handleEnemyCollision(){
        for(auto &enemy : enemies){
            enemy->collide(*player);
        }
    }

    void Level::handleCollectItem(){
        for(auto &item : droppedItems){
            item->collectThis(*player);
        }
    }

    void Level::drawEntities(){
        //set view
        sf::RenderWindow &win=App::instance().win;
        win.setView(STAGE_VIEW);

        //box
        win.draw(S.sceneBox);

        //entities
        for(auto &x : enemyBullets)  x->render(win);
        for(auto &x : playerBullets) x->render(win);
        for(auto &x : enemies)       x->render(win);
        player->render(win);

        //reset view
        win.setView(win.getDefaultView());
    }

    void Level::drawHUD(){
        //hud.box(sf::FloatRect(0,0,128,96),sf::Color::Black,-2.0f,8);
        //hud.box(INGAME_INFO_BOX,sf::Color::Black,1.0f);
        //hud.setPosition(sf::Vector2f(INGAME_INFO_X,INGAME_INFO_Y),false);
        //hud.setPosition(sf::Vector2f(96,GUI::res.font.getLineSpacing(20)/2),true);
        hud.text(sf::Vector2f(64.f,48.f),true,L"中文测试\nASCII Test\nScore:00000000",14);
        hud.image(sf::Vector2f(64.f,48.f),true,"effect_focus");
    }

    void Level::drawLevel(){
        sf::RenderWindow &win=App::instance().win;
        win.clear(sf::Color::White);

        //TODO:draw background

        drawEntities();
        drawHUD();
        if(player)player->drawHUD();

        //TODO:drawother things

        win.display();
    }

    void Level::playerShootBullet(std::unique_ptr<Bullet> &&pb){
        if(levelEnd) return;

        playerBullets.push_back(std::move(pb));
    }

    void Level::enemyShootBullet(std::unique_ptr<Bullet> &&pb){
        if(levelEnd) return;

        enemyBullets.push_back(std::move(pb));
    }

    void Level::dealGlobalDamage(int dmg){
        if(levelEnd) return;

        for(auto &enemy : enemies) enemy->damage(dmg);
    }

    void Level::playerEnterLevel(std::unique_ptr<class Player> &&_pl){
        player.swap(_pl);
        if(player.get() != nullptr)
            levelEnd = false;
    }

    Level::State Level::getLevelState(){
        if(player.get() == nullptr) return lstate_invalid;
        if(levelEnd){
            return player->isDead() ? lstate_failed : lstate_finished;
        }

        return lstate_running;
    }

    void Level::update(sf::Time et){
        if(levelEnd) return;

        //update existing entities
        updateEntities(et);
        handleBulletHit();
        handleEnemyCollision();
        handleCollectItem();
        cleanUp();

        //check next wave
        if(enemies.empty() || allowNextWave){
            if(nextWaveDelay <= sf::Time::Zero) {
                deployEnemy();
            } else {
                nextWaveDelay -= et;
            }
        }

        //check player life
        if(player->isDead()){
            levelEnd = 1;
        }

        //draw!
        drawLevel();

        levelTime += et;
    }

}

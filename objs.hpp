#ifndef __HGUARD_OBJS_OSSBHF
#define __HGUARD_OBJS_OSSBHF

namespace ossbhf{

    class Object{
        private:
            Object(const Object&) =delete;
            Object &operator=(const Object&) =delete;

        public:
            Object() =default;
            virtual ~Object();

            Object(Object&&) =default;
            Object &operator=(Object&&) =default;
    };

    class BaseNoCopy{
        private:
            BaseNoCopy(const BaseNoCopy&) =delete;
            BaseNoCopy &operator=(const BaseNoCopy&) =delete;

        protected:
            BaseNoCopy() =default;
            ~BaseNoCopy() =default;

            BaseNoCopy(BaseNoCopy&&) =default;
            BaseNoCopy &operator=(BaseNoCopy&&) =default;
    };

}

#endif /* __HGUARD_OBJS_OSSBHF */
